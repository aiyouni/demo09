package com.study.demo.compensation;

import org.flowable.bpmn.model.FlowElement;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;

/**
 * 服务任务
 * 
 *
 */
public class ServiceTaskJavaDelegate implements JavaDelegate {

	public void execute(DelegateExecution execution) {
		System.out.println("========================开始执行服务任务,服务任务========================");
		//获取当前节点
		FlowElement flowElement = execution.getCurrentFlowElement();
		System.out.println("当前节点名称: " + flowElement.getName());
		
	}

}
