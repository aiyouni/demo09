package com.study.demo.delegate;

import org.flowable.bpmn.model.FlowElement;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.DelegateHelper;
import org.flowable.engine.delegate.JavaDelegate;

public class CatchExceptionDelegate implements JavaDelegate {

	public void execute(DelegateExecution execution) {
		FlowElement currentFlowElement = DelegateHelper.getFlowElement(execution);
		System.out.println("=====服务任务异常捕获，当前节点： " + currentFlowElement.getName());
		
	}

}
