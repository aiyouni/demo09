package com.study.demo.delegate;

import java.util.concurrent.atomic.AtomicInteger;

import org.flowable.common.engine.api.delegate.Expression;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;

public class PrototypeDelegateExpressionBean implements JavaDelegate {
	public static AtomicInteger INSTANCE_COUNT = new AtomicInteger(0);

	  private Expression fieldA;
	  private Expression fieldB;
	  private Expression resultVariableName;

	  public PrototypeDelegateExpressionBean() {
	    INSTANCE_COUNT.incrementAndGet();
	  }

	  public void execute(DelegateExecution execution) {

	    Number fieldAValue = (Number) fieldA.getValue(execution);
	    Number fieldValueB = (Number) fieldB.getValue(execution);

	    int result = fieldAValue.intValue() + fieldValueB.intValue();
	    execution.setVariable(resultVariableName.getValue(execution).toString(), result);
	    
	    System.out.println("=====================PrototypeDelegateExpressionBean,INSTANCE_COUNT: " + INSTANCE_COUNT);
	  }

}
