package com.study.demo.delegate;

import java.util.concurrent.atomic.AtomicInteger;

import org.flowable.common.engine.api.delegate.Expression;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.DelegateHelper;
import org.flowable.engine.delegate.JavaDelegate;

public class SingletonDelegateExpressionBean implements JavaDelegate {

	public static AtomicInteger INSTANCE_COUNT = new AtomicInteger(0);

	public SingletonDelegateExpressionBean() {
		INSTANCE_COUNT.incrementAndGet();
	}

	public void execute(DelegateExecution execution) {

		Expression fieldAExpression = DelegateHelper.getFieldExpression(execution, "fieldA");
		Number fieldA = (Number) fieldAExpression.getValue(execution);

		Expression fieldBExpression = DelegateHelper.getFieldExpression(execution, "fieldB");
		Number fieldB = (Number) fieldBExpression.getValue(execution);

		int result = fieldA.intValue() + fieldB.intValue();

		String resultVariableName = DelegateHelper.getFieldExpression(execution, "resultVariableName")
				.getValue(execution).toString();
		execution.setVariable(resultVariableName, result);
		
		System.out.println("=====================SingletonDelegateExpressionBean,INSTANCE_COUNT: " + INSTANCE_COUNT);
	}

}
