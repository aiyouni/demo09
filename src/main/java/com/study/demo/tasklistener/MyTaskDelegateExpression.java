package com.study.demo.tasklistener;

import org.flowable.engine.delegate.TaskListener;
import org.flowable.task.service.delegate.DelegateTask;

public class MyTaskDelegateExpression implements TaskListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void notify(DelegateTask delegateTask) {
		System.out.println("=================【当前节点：任务监听器---委托表达式方式】=================================");
		
	}


}
