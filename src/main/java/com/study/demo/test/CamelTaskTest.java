package com.study.demo.test;

import java.util.HashMap;
import java.util.Map;

import org.flowable.engine.DynamicBpmnService;
import org.flowable.engine.FormService;
import org.flowable.engine.HistoryService;
import org.flowable.engine.IdentityService;
import org.flowable.engine.ManagementService;
import org.flowable.engine.ProcessEngine;
import org.flowable.engine.ProcessEngineConfiguration;
import org.flowable.engine.ProcessEngines;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.engine.repository.Deployment;
import org.flowable.engine.repository.DeploymentBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


/**
 *用户任务--测试
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:generic-camel-flowable-context.xml")
public class CamelTaskTest {
	
	protected ProcessEngine processEngine;
	protected TaskService taskService;
	protected RuntimeService runtimeService;
	protected RepositoryService repositoryService;
	protected HistoryService historyService;
	protected DynamicBpmnService dynamicBpmnService;
	protected FormService formService;
	protected IdentityService identityService;
	protected ManagementService managementService;
	protected ProcessEngineConfiguration processEngineConfiguration;

	@Before
	public void testProcessEngine() {
		processEngine = ProcessEngines.getDefaultProcessEngine();
		System.out.println("流程引擎类：" + processEngine);

		taskService = processEngine.getTaskService();
		runtimeService = processEngine.getRuntimeService();
		repositoryService = processEngine.getRepositoryService();
		historyService = processEngine.getHistoryService();
		dynamicBpmnService = processEngine.getDynamicBpmnService();
		formService = processEngine.getFormService();
		identityService = processEngine.getIdentityService();
		managementService = processEngine.getManagementService();
		processEngineConfiguration = processEngine.getProcessEngineConfiguration();

		String name = processEngine.getName();

		System.out.println("流程引擎的名称： " + name);
		System.out.println(processEngineConfiguration);

	}

	/**
	 * 关闭流程引擎
	 */
	@After
	public void close() {
		processEngine.close();
	}

	/**
	 * 部署
	 */
	@Test
	public void deploy() {
		DeploymentBuilder deploymentBuilder = repositoryService
												.createDeployment()
													.category("camelprocess")
													.name("camelprocess")
													.addClasspathResource("process/camel任务.bpmn20.xml");
		Deployment deploy = deploymentBuilder.deploy();

		System.out.println("流程ID: " + deploy.getId());
	}
	/**
	 * 启动流程实例并测试连通性
	 * 
	 */
	@Test
	public void testPingPong() {
        Map<String, Object> variables = new HashMap<String, Object>();

        variables.put("input", "Hello");
        Map<String, String> outputMap = new HashMap<String, String>();
        variables.put("outputMap", outputMap);

        runtimeService.startProcessInstanceByKey("camelprocess", variables);
        System.out.println("==============outputMap: " + outputMap);
    }
	
	/**
	 * 完成任务
	 */
	@Test
	public void complete() {
		String taskId = "160012";
		taskService.complete(taskId);
	}
}
