package com.study.demo.test;

import java.util.HashMap;
import java.util.Map;

import org.flowable.engine.repository.Deployment;
import org.flowable.engine.repository.DeploymentBuilder;
import org.junit.Test;

import com.study.demo.config.BaseConfiguation;


/**
 *用户任务--测试
 *
 */
public class ServiceTaskTest extends BaseConfiguation{

	/**
	 * 部署
	 */
	@Test
	public void deploy() {
		DeploymentBuilder deploymentBuilder = repositoryService
												.createDeployment()
													.category("servicetaks")
													.name("servicetaks")
													.addClasspathResource("process/服务任务.bpmn20.xml");
		Deployment deploy = deploymentBuilder.deploy();

		System.out.println("流程ID: " + deploy.getId());
	}
	/**
	 * 启动流程实例 
	 * 
	 */
	@Test
	public void start() {
		String processDefinitionKey = "servicetaks";
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("myVar", "myVar--test");
		variables.put("gender", "male");
		variables.put("name", "JamesYee");
		variables.put("input", 100);
		variables.put("exception", "throw-exception");
		runtimeService.startProcessInstanceByKey(processDefinitionKey,variables);
	}
	
	/**
	 * 完成任务
	 */
	@Test
	public void complete() {
		String taskId = "390025";
		taskService.complete(taskId);
	}
}
